module gitlab.com/project_falcon/kubeless/klib/toolslego

go 1.14

require (
	gitlab.com/project_falcon/kubeless/lib/payload v1.52.1
	gitlab.com/project_falcon/kubeless/lib/tools v0.10.1
	k8s.io/api v0.19.4
	k8s.io/apimachinery v0.19.4
)

// replace gitlab.com/project_falcon/kubeless/lib/payload => ../../lib/payload
