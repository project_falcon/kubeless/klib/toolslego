package toolslego

import (
	"encoding/json"
	"errors"
	"fmt"
	"strconv"
	"strings"
	"time"

	"gitlab.com/project_falcon/kubeless/lib/payload"
	"gitlab.com/project_falcon/kubeless/lib/tools"
	v1 "k8s.io/api/core/v1"
	meta "k8s.io/apimachinery/pkg/apis/meta/v1"
)

const (
	// //InitContainer is the container which should be put in initcontainer section of poddefinition
	// InitContainer = "init"
	// //RunContainer is the container which should be put in container section and work as a daemon
	// RunContainer = "run"
	// //TestID id for unittest
	// TestID = "test"
	// //KboxID id for run kbox
	// KboxID = "kbox"

	service = "toolslego"

	//TimeSleep is between delete and create POD
	TimeSleep = 1
	//NamespacePrefix prefix which have to added to namespace
	NamespacePrefix = "kbox-"
)

type KConfig struct {
	AccountName  string
	EcrURL       string
	EcrCache     string
	GitRepo      string
	ApiServer    string
	SSHKey       []byte
	Namespace    string
	SuffixDomain string
	TTLBox       int
	IngressURL   string
}

//ReadYAMLfile readin`g YAML file
// func ReadYAMLfile(fileName string) (map[interface{}]interface{}, error) {

// 	m := make(map[interface{}]interface{})
// 	yamlFile, err := ioutil.ReadFile(fileName)
// 	if err != nil {
// 		return m, err
// 	}

// 	err = yaml.Unmarshal(yamlFile, &m)

// 	return m, err
// }

//Int32Ptr function conver INT to the INT pointer
func Int32Ptr(i int32) *int32 {
	return &i
}

//GetLabels get Labels
func GetClusterLabels(apiEvent *payload.APIData) map[string]string {
	labels := map[string]string{
		"cluster":  apiEvent.Cluster.Name,
		"owner":    apiEvent.Cluster.Owner,
		"ttl":      strconv.Itoa(*apiEvent.Cluster.TTL),
		"type":     apiEvent.Cluster.Type,
		"constant": strconv.FormatBool(apiEvent.Cluster.Constant),
		"app":      "falcon",
		"idtask":   apiEvent.IDtask,
		"brand":    apiEvent.Cluster.Brand,
	}

	return labels
}

//GetLabels get Labels
func GetBoxLabels(box payload.Box, podName string) map[string]string {
	labels := map[string]string{
		"app":     podName,
		"branch":  box.Branch.Name,
		"hash":    box.Branch.Hash,
		"brand":   box.Brand,
		"project": box.Project,
	}

	return labels
}

// //ConvertStringToMap converts extraEnv to MAP
// func ConvertExtraEnvToMap(apiEvent *payload.APIData, extraEnv string) (map[string]string, error) {
// 	m := map[string]string{}

// 	err := json.Unmarshal([]byte(extraEnv), &m)
// 	if err != nil {
// 		tools.HandlerMessage(apiEvent, service, fmt.Sprintf("there is problem to UnMarshal trimEnv: '%v' to MAP. Err: '%v'", extraEnv, err.Error()), 500, tools.ErrColor)
// 		return m, err
// 	}

// 	return m, err
// }

// func safeEscape(s string) string {
// 	if len(s) == 0 {
// 		return "''"
// 	}

// 	if pattern.MatchString(s) {
// 		return "'" + strings.ReplaceAll(s, "'", "'\"'\"'") + "'"
// 	}

// 	return s
// }

//ConvertStringToMap converts extraEnv to MAP
func ConvertExtraEnvToMap(apiEvent *payload.APIData, extraEnv []byte) (map[string]string, error) {
	r := make(map[string]json.RawMessage)

	if len(extraEnv) == 0 {
		return make(map[string]string), nil
	}

	err := json.Unmarshal(extraEnv, &r)
	if err != nil {
		tools.HandlerMessage(apiEvent, service, fmt.Sprintf("there is problem to UnMarshal extraEnv '%v'to json.RawMessage. Err: '%v'", string(extraEnv), err.Error()), 500, tools.ErrColor)
	}

	m := map[string]string{}

	for key, val := range r {
		text := strings.TrimLeft(string(val), "\"")
		text = strings.TrimRight(text, "\"")
		// m[key] = shellescape.Quote(text)
		m[key] = text
	}

	return m, err
}

//MergeMaps 2 maps[string]string in one // maybe switch to  maps[string]interface
func MergeMaps(m1 map[string]string, m2 map[string]string) map[string]string {

	for key, val := range m2 {
		m1[key] = val
	}

	return m1
}

// func GetClusterName(apiEvent *payload.APIData) string {
// 	return strings.ToLower(fmt.Sprintf("%v-%v-%v", apiEvent.Cluster.Type, apiEvent.Cluster.Name, apiEvent.IDtask))
// }

// //ConvertMapToLabels conver labels of MAP to labels of payload.Labels type
func ConvertMapToLabels(apiEvent *payload.APIData, l map[string]string) (*payload.Labels, error) {

	var labels payload.Labels

	tmpLabels, err := json.Marshal(l)
	if err != nil {
		tools.HandlerMessage(apiEvent, service, fmt.Sprintf("Cannot Marshal val.Labels. Err: '%v'", err.Error()), 500, tools.ErrColor)
		return nil, err
	}

	if err := json.Unmarshal(tmpLabels, &labels); err != nil {
		tools.HandlerMessage(apiEvent, service, fmt.Sprintf("Cannot UnMarshal val.Labels. Err: '%v'", err.Error()), 500, tools.ErrColor)
		return nil, err
	}
	// fmt.Println("labelsy", labels)

	// 	if l["build"] != "" {
	// 		build, _ := strconv.Atoi(l["build"])
	// 		labels.BuildInt = &build
	// 	}

	// 	m := make(map[string]string)
	// 	for key, val := range l {
	// 		if strings.HasPrefix(key, "extraenv.") {
	// 			m[strings.TrimPrefix(key, "extraenv.")] = val
	// 		}
	// 	}

	// 	if len(m) > 0 {
	// 		labels.ExtraEnv = m
	// 	}

	return &labels, nil
}

// //ConvertMapToString conver map to string
// func ConvertMapToString(m map[string]string) string {
// 	b := new(bytes.Buffer)

// 	for key, val := range m {
// 		fmt.Fprintf(b, "%s=\"%s\",", key, val)
// 	}

// 	return strings.TrimSuffix(b.String(), ", ")
// }

//GetTypeBox get type of JOB
func GetTypeBox(action string) string {
	var boxType string

	switch action {

	case payload.CK8sCreateUnitTest:
		boxType = "unittest"

	case payload.CK8sCreateBuild:
		boxType = "build"

	case payload.CK8sCreateBox, payload.DK8sCreateDeploy, payload.DK8sUpdateDeploy:
		boxType = "kbox"
	}

	return boxType
}

//GetBaseName get base name for some k8s resource
func GetBaseName(action string, projectName string) string {
	return fmt.Sprintf("%v-%v", GetTypeBox(action), projectName)
}

// //GetPODName get POD name
// // func GetPODName(typeJob string, box *payload.Box, data) string {
// func GetPODName(jobType string, apiEvent *payload.APIData, box *payload.Box) string {

// 	podName := strings.ToLower(fmt.Sprintf("%v-%v-%v-%v", jobType, box.Project, box.Brand, apiEvent.IDtask))

// 	if len(podName) > 253 {
// 		podName = podName[:253]
// 	}

// 	return podName
// }

//GetNameSpace get namespace for operation
// func GetNameSpace(apiEvent *payload.APIData) string {

// 	if apiEvent.K8s.NamespaceID == "" {
// 		return "default"
// 	}

// 	return apiEvent.K8s.NamespaceID
// }

//GetListOptions create filter for resources
func GetListOptions(filter string) meta.ListOptions {
	listOptions := meta.ListOptions{
		LabelSelector: filter,
	}

	return listOptions
}

//TimeParseRFC3339 parse time to RFC3339
func TimeParseRFC3339(apiEvent *payload.APIData, t *meta.Time) (time.Time, error) {

	tf, err := time.Parse(time.RFC3339, t.UTC().Format(time.RFC3339))
	if err != nil {
		tools.HandlerMessage(apiEvent, service, fmt.Sprintf("Cannot parse Time correctly '%v'. Err. '%v'", t.String(), err.Error()), 201, tools.WarnColor)
	}

	return tf, err
}

//IfPodExist check if pod exist in list
func IfPodExist(podName string, podList []payload.Pods) bool {
	exist := false
	for _, pod := range podList {
		if pod.Name == podName {
			exist = true
		}
	}
	return exist
}

//IfIngressExist check if Ingress exist in list
func IfIngressExist(ingressName string, ingressList []payload.Ingress) bool {
	exist := false
	for _, ingress := range ingressList {
		if ingress.Name == ingressName {
			exist = true
		}
	}
	return exist
}

//IfServiceExist check if Service exist in list
func IfServiceExist(serviceName string, serviceList []payload.Services) bool {
	exist := false
	for _, service := range serviceList {
		if service.Name == serviceName {
			exist = true
		}
	}
	return exist
}

//IfNamespaceExist check if Namespace exist in list
func IfNamespaceExist(namespaceName string, namespaceList []payload.Namespace) bool {
	exist := false
	for _, namespace := range namespaceList {
		if namespace.Name == namespaceName {
			exist = true
		}
	}
	return exist
}

//FindNamespace find and return namespace
func FindNamespace(namespaceName string, namespaceList []payload.Namespace) (payload.Namespace, error) {
	for _, namespace := range namespaceList {
		if namespace.Name == namespaceName {
			return namespace, nil
		}
	}
	return payload.Namespace{}, fmt.Errorf("thre is not a namespace %v", namespaceName)
}

//IfConfigMapExist check if ConfigMap exist in list
func IfConfigMapExist(configmapName string, configmapList []payload.ConfigMap) bool {
	exist := false
	for _, configmap := range configmapList {
		if configmap.Name == configmapName {
			exist = true
		}
	}
	return exist
}

//FindConfigMap find and return configmap
func FindConfigMap(configMapName string, configMapList []payload.ConfigMap) (payload.ConfigMap, error) {
	for _, configmap := range configMapList {
		if configmap.Name == configMapName {
			return configmap, nil
		}
	}
	return payload.ConfigMap{}, fmt.Errorf("thre is not a configMap %v", configMapName)
}

//IfSecretExist check if Secret exist in list
func IfSecretExist(secretName string, secretList []payload.Secret) bool {
	exist := false
	for _, secret := range secretList {
		if secret.Name == secretName {
			exist = true
		}
	}
	return exist
}

//IfDeployExist check if Secret exist in list
func IfDeployExist(deployName string, deployList []payload.Deploy) bool {
	exist := false
	for _, deploy := range deployList {
		if deploy.Name == deployName {
			exist = true
		}
	}
	return exist
}

//CheckNamespace check if namespace is not forbidden
func CheckNamespace(namespace string) string {
	if namespace == "" {
		return "default"
	}

	if strings.HasPrefix(namespace, NamespacePrefix) {
		return namespace
	}

	return fmt.Sprintf("%v%v", NamespacePrefix, namespace)
}

//GetNamespace generate the namespace
func GetNamespace(apiEvent *payload.APIData) (string, error) {
	apiEvent.Cluster = apiEvent.Cluster.VerifyStruct()

	if apiEvent.Cluster.Type == "" {
		message := "apiEvent.Cluster.Type is required"
		tools.HandlerMessage(apiEvent, service, message, 421, tools.ErrColor)
		return "", errors.New(message)
	}

	if apiEvent.Cluster.Name == "" {
		message := "apiEvent.Cluster.Name is required"
		tools.HandlerMessage(apiEvent, service, message, 421, tools.ErrColor)
		return "", errors.New(message)
	}

	if apiEvent.IDtask == "" {
		message := "apiEvent.IDtask is required"
		tools.HandlerMessage(apiEvent, service, message, 421, tools.ErrColor)
		return "", errors.New(message)
	}

	clusterID := apiEvent.Cluster.Name

	cut := 63 - len(apiEvent.Cluster.Type) - tools.IDTaskLenght - 2 // minus 2 slash

	if len(clusterID) > cut {
		clusterID = clusterID[:cut]
	}

	return strings.ToLower(fmt.Sprintf("%v-%v-%v", apiEvent.Cluster.Type, clusterID, apiEvent.IDtask)), nil
}

func GetPodStatus(podstat v1.PodStatus) *payload.PodStatus {

	podStatus := payload.PodStatus{}

	for _, contStat := range podstat.Conditions {

		s := payload.StepStatus{
			Status:  string(contStat.Status),
			Reason:  contStat.Reason,
			Message: contStat.Message,
		}

		if !contStat.LastProbeTime.Time.IsZero() {
			s.LastProbeTime = &contStat.LastProbeTime.Time
		}

		if !contStat.LastTransitionTime.Time.IsZero() {
			s.LastTransitionTime = &contStat.LastTransitionTime.Time
		}

		if contStat.Type == "Initialized" {
			podStatus.Initialized = &s
		}

		if contStat.Type == "Ready" {
			podStatus.Ready = &s
		}

		if contStat.Type == "ContainersReady" {
			podStatus.ContainersReady = &s
		}

		if contStat.Type == "PodScheduled" {
			podStatus.PodScheduled = &s
		}
	}

	podStatus.Phase = string(podstat.Phase)

	return &podStatus
}

// func GetPodStatus(podstatus v1.PodStatus) payload.Status {

// 	ready := true
// 	status := "Ok"
// 	exitcode := 0

// 	if podstatus.Phase == "Running" {
// 		for _, contStat := range podstatus.ContainerStatuses {
// 			if !contStat.Ready {
// 				ready = false
// 				status = contStat.LastTerminationState.Terminated.Reason
// 				exitcode = int(contStat.LastTerminationState.Terminated.ExitCode)
// 			}
// 			//  fmt.Println("stat",  contStat.LastTerminationState)
// 		}
// 	}

// 	// tt,_ := json.Marshal(podstatus)
// 	// fmt.Println("stat", string(tt))

// 	if podstatus.Phase == "Pending" {
// 		status = "Pending"
// 	}

// 	if podstatus.Phase == "Failed" {

// 		status = "Failed"
// 		checkCondition(podstatus.Conditions)
// 	}

// 	statusPod := payload.Status{
// 		Status:   status,
// 		Ready:    &ready,
// 		ExitCode: &exitcode,
// 	}

// 	// status := payload.Status{}

// 	return statusPod
// }
